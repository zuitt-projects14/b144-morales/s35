const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")

//Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const userData = 
		auth.decode(req.headers.authorization)
	

/*const userData = auth.decode(req.headers.authorization)	*/


	courseController.addCourse(req.body, {userData: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})



//retrive all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

//Retrieve all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

//retrieve specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
}) 


//update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//Activity s35

/*router.patch("/:courseId", auth.verify, (req, res) => {
	courseController.archieveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})*/


router.put("/:courseId/details", auth.verify, (req, res) => {
	courseController.archieveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})



module.exports = router;






















